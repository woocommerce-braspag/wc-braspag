# Etapas do Projeto


- [x] Implementação do ambiente para acompanhamento
- [X] Estudo da documentação p/ API braspeg
- [X] Desenvolvimento de SDK para integração plugin e API (onboarding) - Falta o retorno da Braspag
- [X] Desenvolvimento de SDK para integração plugin e API (Crédito e Debito) 
- [X] Implementação do diretorio inicial do plugin
- [X] Inserção de campos braspeg na tela de edição dos vendedores (admin)
- [X] Integração das informações dos vendedores (dokan) na API braspeg split
- [X] Testes de integração e conformidade
- [X] Inserção de campos de pagamento no checkout (Cartão de cretdito e Debito)
- [X] Desenvolvimento da tela administrativa para o gerenciamento das formas de pagamento
- [X] Integração para pagamentos em Cartão de cretdito
- [X] Testes de integração e conformidade
- [X] Integração para pagamentos em Cartão de Debito
- [X] Testes de integração e conformidade
- [X] Ajustes e revisões finais para telas adminstrativas
- [X] Ajustes e revisões finais para as formas de pagamento 
- [ ] Testes Finais e Ajustes
- [ ] Implementação do plugin na loja do cliente
- [ ] Homologação junto a braspeg e solicitação das credenciais de produção
- [ ] Ajustes 
- [ ] Entrega
